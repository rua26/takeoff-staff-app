import React, {useEffect} from 'react';
import {connect} from 'react-redux';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import {updateProfile} from './actions/profile';

import Home from './components/Home/Home';
import Login from './components/Login/Login';

const App = ({isAuthenticated, updateProfile}) => {
  //Redirect to login page if user didn't log in
  const PrivateRoute = ({ component: Component, ...rest}) => {
    return (
      <Route
        {...rest}
        render={props =>
          isAuthenticated ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
        }
      />
    )
  };

  useEffect(() => {
    const token = sessionStorage.getItem('token');
    if (token) {
      updateProfile({isAuthenticated: true});
    }
  }, []);

  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </Router>
  );
}

function mapStateToProps(state, props) {
    return {
      isAuthenticated: state.profileReducer.isAuthenticated,
    }
}

export default connect(mapStateToProps, {updateProfile})(App);
