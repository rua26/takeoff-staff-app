import {
    UPDATE_PROFILE,
    REMOVE_PROFILE,
} from '../actionTypes';

const initialState = {accessToken: '', isAuthenticated: false};

const profileReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_PROFILE:
            return {...state, ...action.payload};
        case REMOVE_PROFILE:
            return initialState;
        default:
            return state;
    }
}

export default profileReducer;