import {
    UPDATE_CONTACT,
    REMOVE_CONTACT,
    ADD_CONTACT,
} from '../actionTypes';

import Constant from '../Constant';

const initialState = Constant.INITIAL_DATA;

const contactsReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_CONTACT: {
            const { id, name, email } = action.payload;
            return [
                ...state,
                {
                    id: id,
                    name: name,
                    email: email
                }
            ];
        }
        case UPDATE_CONTACT: {
            const { id, name, email } = action.payload;
            return state.map(contact =>
                (contact.id === id)
                    ? {...contact, name: name, email: email}
                    : contact
            );
        }
        case REMOVE_CONTACT: {
            const id = action.payload;
            const newContacts = state.filter((contact) => {
                return contact.id !== id;
            });
            return newContacts;
        }
        default:
            return state;
    }
}

export default contactsReducer;