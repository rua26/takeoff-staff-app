import {combineReducers} from 'redux';
import profileReducer from './profile';
import contactsReducer from './contacts';
const rootReducer = combineReducers({
    profileReducer,
    contactsReducer,
});

export default rootReducer;