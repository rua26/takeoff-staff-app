import {
    REMOVE_PROFILE,
    UPDATE_PROFILE
} from '../actionTypes';

export const updateProfile = data => ({
    type: UPDATE_PROFILE,
    payload: data,
})

export const removeProfile = () => ({
    type: REMOVE_PROFILE,
})