import {
    ADD_CONTACT,
    UPDATE_CONTACT,
    REMOVE_CONTACT,
} from '../actionTypes';

let actionId = 2;

export const addContact = contact => ({
    type: ADD_CONTACT,
    payload: {
        id: ++actionId,
        ...contact,
    },
});

export const updateContact = contact => ({
    type: UPDATE_CONTACT,
    payload: contact
});

export const removeContact = id => ({
    type: REMOVE_CONTACT,
    payload: id,
});
