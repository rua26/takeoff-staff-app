export default class Constant {
    static PRIMARY_COLOR = '#1578d5';
    static HIGHLIGHT_COLOR = '#f8bc14';
    static BORDER_COLOR = '#dcdee6';
    static LIGHT_GRAY_COLOR = '#dcdfe0';
    static DARK_GRAY_COLOR = '#97a2ad';

    static INITIAL_DATA = [
        {id: 1, name: "Tom", email: "tom@gmail.com"},
        {id: 2, name: "Kim", email: "kim@gmail.com"},
    ];
};