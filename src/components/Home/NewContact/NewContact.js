import React, {Component} from 'react';

import {connect} from 'react-redux';

import {addContact} from '../../../actions/contacts';

class NewContact extends Component {
    constructor(props) {
        super(props);

        this.initialState = {
            name: '',
            email: ''
        };

        this.state = this.initialState;
    }

    //Handle change of contact
    handleChangeContact = event => {
        const { name, value } = event.target;

        this.setState({
            [name] : value
        });
    }

    // Add new contact to table
    onFormSubmit = (event) => {
        event.preventDefault();

        this.props.addContact(this.state);
        this.setState(this.initialState);
    }

    render() {
        const { name, email } = this.state;

        return (
            <div>
                <h3>Add new: </h3>
                <form onSubmit={this.onFormSubmit}>
                    <div>
                        <label>Name</label>
                        <input
                            type="text"
                            name="name"
                            value={name}
                            placeholder="Your name"
                            onChange={this.handleChangeContact}
                            className='form-control'
                            required
                        />
                    </div>
                    <div className='form-group'>
                        <label>Email</label>
                        <input
                            type="email"
                            name="email"
                            value={email}
                            placeholder="Your contact"
                            onChange={this.handleChangeContact}
                            className='form-control'
                            required
                        />
                    </div>
                    <button type="submit" className='btn btn-primary'>Save</button>
                </form>
            </div>
        );
    }
}

function mapStateToProps(state, props) {
    return {}
}

export default connect(mapStateToProps, {addContact})(NewContact);