import React, {useState, useEffect} from 'react';

import {connect} from 'react-redux';

import ContactList from './ContactList/ContactList';
import NewContact from './NewContact/NewContact';

import {removeProfile} from '../../actions/profile';

import styles from './styles.js';

const Home = ({contacts, removeProfile}) => {
  const [contactsData, setContactsData] = useState(contacts)

  useEffect(() => {
    setContactsData(contacts)
  }, [contacts]);

  //Search contact by data entered by the user
  const searchContactByText = (e) => {
    e.preventDefault();

    const keyWord = e.target.value;
    const result =  contacts.filter(contact => {
        if (contact.name.toLowerCase().indexOf(keyWord.toLowerCase()) > -1) {
            return true;
        }
        return false;
    });
    setContactsData(result);
  };

  //Logout user and remove token from storage
  const logOut = () => {
    sessionStorage.removeItem('token');
    removeProfile();
  }

  return (
    <div style={styles.container}>
      <button onClick={() => logOut()} className='btn btn-danger'>Logout</button>
      <h1 className='text-center'>React app</h1>
      <div className='form-group'>
        <input
            type='text'
            onChange={(e) => searchContactByText(e)}
            placeholder="Search by name"
            className='form-control'
        />
      </div>
      <ContactList contacts={contactsData} />
      <NewContact />
    </div>
  );
};

function mapStateToProps(state, props) {
    return {
      contacts: state.contactsReducer,
    }
}

export default connect(mapStateToProps, {removeProfile})(Home);
