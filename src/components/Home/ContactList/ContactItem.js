import React, {useState} from 'react';

import {connect} from 'react-redux';

import {
    removeContact,
    updateContact,
} from '../../../actions/contacts';

export const ContactItem = ({contact, removeContact, updateContact}) => {
    const [editContact, setEditContact] = useState({});
    const [edit, setEdit] = useState(false);

    //Update change
    const handleEditChange = (e) => {
        setEditContact({...editContact, [e.target.name]: e.target.value})
    }

    //Save change after modify
    const saveChange = () => {
        updateContact(editContact);
        setEdit(false);
    }

    return (
        <>
            {!edit ? (
                <tr key={contact.id}>
                    <td>{contact.name}</td>
                    <td>{contact.email}</td>
                    <td><button className='btn btn-danger' onClick={() => removeContact(contact.id)}>Delete</button></td>
                    <td>
                        <button
                            className='btn btn-primary'
                            onClick={() => {
                                setEdit(true);
                                setEditContact(contact);
                            }
                        }>Edit</button>
                    </td>
                </tr>
            ) : (
                <tr>
                    <td>
                        <input
                            type="text"
                            name="name"
                            id="name"
                            value={editContact.name}
                            onChange={(e) => handleEditChange(e)}
                            className='form-control'
                            required
                        />
                    </td>
                    <td>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            value={editContact.email}
                            onChange={(e) => handleEditChange(e)}
                            className='form-control'
                            required
                        />
                    </td>
                    <td><button className='btn btn-danger' onClick={() => removeContact(contact.id)}>Delete</button></td>
                    <td><button className='btn btn-primary' onClick={() => saveChange()}>Save</button></td>
                </tr>
            )}
        </>
    )
}

function mapStateToProps(state, props) {
    return {
        contacts: state.contactsReducer,
    }
}

export default connect(mapStateToProps, {removeContact, updateContact})(ContactItem);