import React from 'react';

import {connect} from 'react-redux';

import ContactItem from './ContactItem';

const TableHeader = () => {
    return (
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Remove</th>
                <th scope="col">Edit</th>
            </tr>
        </thead>
    );
};

const ContactList = ({contacts}) => {
    const rows = contacts.map((row, index) => {
        return (
            <ContactItem contact={row} key={index}/>
        )
    });

    return (
        <table className='table table-striped'>
            <TableHeader />
            <tbody>{rows}</tbody>
        </table>
    );
}

function mapStateToProps(state, props) {
    return {}
}

export default connect(mapStateToProps)(ContactList);