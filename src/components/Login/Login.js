import React , {useState} from 'react';

import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';

import {updateProfile} from '../../actions/profile';

import styles from './styles';

const Login = ({updateProfile, location, isAuthenticated}) => {
    const { from } = location.state || { from: { pathname: '/' } };

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    //Fake authentication with user from users.json
    const loginWithEmailAndPassword = (e) => {
        e.preventDefault();

        let url = 'http://localhost:8080/login';
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email,
                password: password,
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data.status === 200) {
                const token = data.access_token;
                sessionStorage.setItem('token', token);
                const result = {
                    isAuthenticated: true,
                    accessToken: token,
                };
                updateProfile(result);
            } else {
                const error = new Error(data.error);
                throw error;
            }
        })
        .catch(err => {
            console.error(err);
            alert('Error logging in please try again');
        });
    };

    if (isAuthenticated) {
        return <Redirect to={from} />;
    };

    return (
        <div style={styles.container}>
            <h2 className='text-center'>Login form</h2>
            <form onSubmit={(e) => loginWithEmailAndPassword(e)} className='form-group'>
                <div className='form-group'>
                    <label>Email address:</label>
                    <input
                        type='text'
                        value={email}
                        name='email'
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder='Your email'
                        className='form-control'
                    />
                </div>
                <div className='form-group'>
                    <label>Password</label>
                    <input
                        type='password'
                        value={password}
                        name='password'
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder='Your password'
                        className='form-control'
                    />
                </div>
                <button type="submit" className='btn btn-primary'>Login</button>
            </form>
        </div>
    )
};


function mapStateToProps(state, props) {
    return {
      isAuthenticated: state.profileReducer.isAuthenticated,
    }
}


export default connect(mapStateToProps, {updateProfile})(Login);