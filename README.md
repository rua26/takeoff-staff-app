# Built with
ReactJs

# Instruction to run the app

To start the app you should following these steps;

1. First, clone the repo via git:

```bash
git clone https://gitlab.com/rua26/takeoff-staff-app.git
```

2. Install dependencies with npm. Open your terminal and run:

```bash
cd takeoff-staff-app
npm install
```

3. Run fake REST API using json-server with JWT authentication:

```bash
npm run server
```

4. Open a new terminal in the current directory and run the app in the development mode.

```bash
npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

5. You can login to the app with the following data.

```
{
  "email": "messi@email.com",
  "password":"messi"
}
```
